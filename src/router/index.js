import { createRouter, createWebHistory } from 'vue-router';

import { useAuthStore } from '@/stores/auth.store';
import LoginPage from '@/views/LoginPage.vue';
import DashboardPage from '@/views/DashboardPage.vue';

export const router = createRouter({
	history: createWebHistory(import.meta.env.BASE_URL),
	linkActiveClass: 'active',
	routes: [
		{ path: '/', component: DashboardPage, meta: {requiresAuth: true} },
		{ path: '/login', component: LoginPage,  meta: {requiresAuth: false} }
	]
});

// router.beforeEach(async (to) => {
// 	// redirect to login page if not logged in and trying to access a restricted page
// 	const publicPages = ['/login'];
// 	const authRequired = !publicPages.includes(to.path);
// 	const auth = useAuthStore();
//
// 	if (authRequired && !auth.token) {
// 		auth.returnUrl = to.fullPath;
// 		return '/login';
// 	}
// });

router.beforeEach((to) => {
	const store = useAuthStore()

	if (to.meta.requiresAuth && !store.token) return '/login'
})