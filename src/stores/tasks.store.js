import {defineStore} from 'pinia';

import {fetchWrapper} from '@/helpers/index.js';

const baseUrl = `${import.meta.env.VITE_API_URL}/api`;

export const useTasksStore = defineStore({
	id: 'tasks',
	state: () => ({
		tasks: {},
		urgentTasks: {},
	}),
	actions: {
		async getAll() {
			this.tasks = { loading: true };
			fetchWrapper.get(baseUrl + '/tasks/list')
				.then((tasks) => {
					let tasksList = tasks.data;
					this.tasks = tasksList.filter(task => task.is_urgent !== 1);
					this.urgentTasks = tasksList.filter(task => task.is_urgent === 1);
				})
				.catch(error => this.tasks = { error })
		},
		async add(values) {
			fetchWrapper.post(baseUrl + '/tasks/create', values)
				.then((task) => {
					let taskItem = task.data;
					taskItem.is_urgent === true ? this.urgentTasks.push(taskItem) : this.tasks.push(taskItem);
				})
				.catch(error => this.tasks = {error})
		},

		async delete(id) {
			fetchWrapper.delete(baseUrl + '/tasks/' + id)
				.then(() => {
					this.tasks = this.tasks.filter(task => task.id !== id)
					this.urgentTasks = this.urgentTasks.filter(task => task.id !== id)
				})
				.catch(error => this.tasks = { error })
		},

		async editStatus(id, status) {
			fetchWrapper.post(baseUrl + `/tasks/${id}/edit-status`, {is_urgent: status})
				.then((task) => {
					console.log(status);

					status ? this.tasks =this.tasks.filter(task => task.id !== id) : this.urgentTasks = this.urgentTasks.filter(task => task.id !== id);

					let taskItem = task.data;
					taskItem.is_urgent === true ? this.urgentTasks.push(taskItem) : this.tasks.push(taskItem);
				})
				.catch(error => this.tasks = { error })
		}
	}
});