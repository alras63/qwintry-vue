import { defineStore } from 'pinia';

import { fetchWrapper } from '@/helpers/index.js';

import {router} from "@/router";

const baseUrl = `${import.meta.env.VITE_API_URL}/api`;

export const useAuthStore = defineStore({
	id: 'auth',
	state: () => ({
		token: JSON.parse(localStorage.getItem('token')),
		returnUrl: null
	}),
	actions: {
		async login(email, password) {
			const token = await fetchWrapper.post(`${baseUrl}/auth/login`, { email, password });
			this.token = token;
			localStorage.setItem('token', JSON.stringify(token));
			await router.push(this.returnUrl || '/');
		},
		logout() {
			this.token = null;
			localStorage.removeItem('token');
			router.push('/login').then(r => console.info('logout'));
		}
	}
});